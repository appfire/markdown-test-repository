# Public examples

## Tables

| foo | bar |
| --- | --- |
| baz | bim |

## Code blocks

```JSON
{
"name" : "simple",
"description" : "simple json test",
"cnt" : 8
}
```